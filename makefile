limpiar:
	rm -rf bin

compilar:limpiar
	mkdir bin
	javac src/contactos/Contacto.java -d bin
	javac -cp bin src/contactos/Libreta.java -d bin
	javac -cp bin src/aplicacion/Principal.java -d bin

jar:compilar
	jar cvfm libretaContactos.jar manifest.txt -C bin .

ejecutar:jar
	java -cp bin aplicacion.Principal
