package aplicacion;
import contactos.*;

public class Principal{
	public static void main(String[]args){
		Libreta libretaPrincipal=new Libreta();
		libretaPrincipal.setNombre("libreta principal");
		Contacto jose=new Contacto();
		Contacto maria=new Contacto();
		Contacto juan=new Contacto();
		Contacto pepe=new Contacto();
		jose.setNombre("José");
		maria.setNombre("María");
		juan.setNombre("Juan");
		pepe.setNombre("Pepe");
		jose.setNumeroTelefono("685479123");
		maria.setNumeroTelefono("653142618");
		juan.setNumeroTelefono("648151326");
		pepe.setNumeroTelefono("642121485");
		libretaPrincipal.annadir(jose);
		libretaPrincipal.annadir(maria);
		libretaPrincipal.annadir(juan);
		libretaPrincipal.annadir(pepe);
		System.out.println(libretaPrincipal);
	}
}
