package contactos;

public class Contacto{
	private String nombre;
	private String numeroTelefono;

	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	
	public String getNombre(){
		return nombre;
	}

	public void setNumeroTelefono(String numeroTelefono){
		this.numeroTelefono = numeroTelefono;
	}

	public String getNumeroTelefono(){
		return numeroTelefono;
	}

	@Override
	public String toString(){
		return getNombre() + ", número de teléfono: " + getNumeroTelefono();
	}
} 
